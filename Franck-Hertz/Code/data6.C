void data6(){

  ifstream file;
  file.open("Data/166-1.60-1.17.txt"); //Input file

  if(!file) { //File couldn't be opened
    cerr << "Error: File could not be opened." << endl;
    exit(1);
  }

  float temp1, temp2, temp3, curr_el, u2_el;
  string temp4, freq;

  float curr_arr[151];
  float u2_arr[151];

  int count=0;

  while(file.good()){
    if(count<5){
      getline(file,temp4,'\n');
    }
    else{
      file >> temp1 >> temp2 >> temp3 >> curr_el >> u2_el >> freq;
      cout << "Current (I): " << curr_el << " Accelerating Voltage (U2): " << u2_el << endl;
        
      curr_arr[count-5]=curr_el;
      u2_arr[count-5]=u2_el;
    }
    count++;
  }

  cout << "Number of lines: " << count << endl; //In order to check if we read the file correctly
  file.close(); //We should always close the file

  //Canvas Creation and Modification
  TCanvas *c1 = new TCanvas();
  c1->SetTickx();
  c1->SetTicky();
  c1->SetGridx();
  c1->SetGridy();

  //Error Bars of U2 and I
  float u2_err[151], curr_err[151];

  for(int i=0; i<151; i++){
    u2_err[i] = 0.05;
    curr_err[i] = 0.005;
  }

  //Plotting the U2 vs. I Graph
  TGraphErrors *gr = new TGraphErrors(151, u2_arr, curr_arr, u2_err, curr_err);

  gr->SetMarkerColor(kBlue);
  gr->SetLineWidth(1);
  gr->SetTitle("Accelerating Voltage (U2) vs. Current (I)");
  gr->GetXaxis()->SetTitle("U2 (V)");
  gr->GetYaxis()->SetTitle("I (A)");
  gr->Draw("A*");

  //Fitting Gaussian Distribution to each peak
  //Fit ranges are decided by inspecting the peaks from the plot
  TF1 *gauss_1 = new TF1("gauss_1", "gaus", 9.0, 10.2); 
  TF1 *gauss_2 = new TF1("gauss_2", "gaus", 13.8, 15.2); 
  TF1 *gauss_3 = new TF1("gauss_3", "gaus", 19.0, 20.3); 
  TF1 *gauss_4 = new TF1("gauss_4", "gaus", 24.0, 25.7); 
  TF1 *gauss_5 = new TF1("gauss_5", "gaus", 28.9, 30.8);

  gauss_1->SetLineWidth(3);
  gauss_2->SetLineWidth(3);
  gauss_3->SetLineWidth(3);
  gauss_4->SetLineWidth(3);
  gauss_5->SetLineWidth(3);

  gauss_1->SetLineColor(kRed);
  gauss_2->SetLineColor(kRed);
  gauss_3->SetLineColor(kRed);
  gauss_4->SetLineColor(kRed);
  gauss_5->SetLineColor(kRed);

  gr->Fit(gauss_1, "R");
  gr->Fit(gauss_2, "R+");
  gr->Fit(gauss_3, "R+");
  gr->Fit(gauss_4, "R+");
  gr->Fit(gauss_5, "R+");

  //Getting the mean and sigma values of the Gaussian Fits with their corresponding uncertainties
  float gauss1_mean = gauss_1->GetParameter(1);
  float gauss1_sigma =gauss_1->GetParameter(2);
  float gauss1_mean_un = gauss_1->GetParError(1);
  float gauss1_sigma_un = gauss_1->GetParError(2);

  float gauss2_mean = gauss_2->GetParameter(1);
  float gauss2_sigma =gauss_2->GetParameter(2);
  float gauss2_mean_un = gauss_2->GetParError(1);
  float gauss2_sigma_un = gauss_2->GetParError(2);

  float gauss3_mean = gauss_3->GetParameter(1);
  float gauss3_sigma =gauss_3->GetParameter(2);
  float gauss3_mean_un = gauss_3->GetParError(1);
  float gauss3_sigma_un = gauss_3->GetParError(2);

  float gauss4_mean = gauss_4->GetParameter(1);
  float gauss4_sigma =gauss_4->GetParameter(2);
  float gauss4_mean_un = gauss_4->GetParError(1);
  float gauss4_sigma_un = gauss_4->GetParError(2);

  float gauss5_mean = gauss_5->GetParameter(1);
  float gauss5_sigma =gauss_5->GetParameter(2);
  float gauss5_mean_un = gauss_5->GetParError(1);
  float gauss5_sigma_un = gauss_5->GetParError(2);

  //Finding the difference between each peak
  float delta_E1 = gauss2_mean - gauss1_mean;
  float delta_E2 = gauss3_mean - gauss2_mean;
  float delta_E3 = gauss4_mean - gauss3_mean;
  float delta_E4 = gauss5_mean - gauss4_mean;

  //Finding the uncertainty of each peak
  float sigma_E1 = sqrt(pow(gauss1_mean_un,2)+pow(gauss2_mean_un,2));
  float sigma_E2 = sqrt(pow(gauss2_mean_un,2)+pow(gauss3_mean_un,2));
  float sigma_E3 = sqrt(pow(gauss3_mean_un,2)+pow(gauss4_mean_un,2));
  float sigma_E4 = sqrt(pow(gauss4_mean_un,2)+pow(gauss5_mean_un,2));
  
  //Finding the weight of each delta E value
  float deltaE_w1 = 1 / pow(sigma_E1,2);
  float deltaE_w2 = 1 / pow(sigma_E2,2);
  float deltaE_w3 = 1 / pow(sigma_E3,2);
  float deltaE_w4 = 1 / pow(sigma_E4,2);

  float deltaE_wsum = deltaE_w1 + deltaE_w2 + deltaE_w3 + deltaE_w4;

  //Finding the final delta E value
  float delta_E = (delta_E1*deltaE_w1 + delta_E2*deltaE_w2 + delta_E3*deltaE_w3 + delta_E4*deltaE_w4) / deltaE_wsum;

  //Finding the final sigma E value
  float sigma_E = sqrt(1/deltaE_wsum);
  
  //PRINTING PART
  
  //Printing out the mean values of each Gaussian Fit
  cout << "Gauss 1 Mean: " << gauss1_mean << " +/- " << gauss1_mean_un << endl;
  cout << "Gauss 2 Mean: " << gauss2_mean << " +/- " << gauss2_mean_un << endl;
  cout << "Gauss 3 Mean: " << gauss3_mean << " +/- " << gauss3_mean_un << endl;
  cout << "Gauss 4 Mean: " << gauss4_mean << " +/- " << gauss4_mean_un << endl;
  cout << "Gauss 5 Mean: " << gauss5_mean << " +/- " << gauss5_mean_un << endl;

  //Printing out the delta E values
  cout << "Delta E1: " << delta_E1 << endl;
  cout << "Delta E2: " << delta_E2 << endl;
  cout << "Delta E3: " << delta_E3 << endl;
  cout << "Delta E4: " << delta_E4 << endl;

  //Printing out the uncertainty of each peak
  cout << "Sigma E1: " << sigma_E1 << endl;
  cout << "Sigma E2: " << sigma_E2 << endl;
  cout << "Sigma E3: " << sigma_E3 << endl;
  cout << "Sigma E4: " << sigma_E4 << endl;

  //Printing out the weighted mean of the delta E values
  cout << "Weighted mean of delta E values: " << delta_E << endl;

  //Printing out the uncertainty of the weighted mean of delta E values
  cout << "Mean of sigma E values: " << sigma_E << endl;
}
