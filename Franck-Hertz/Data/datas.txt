//166-1.60-1.61
Gauss 1 Mean: 9.37263 +/- 0.0206773
Gauss 2 Mean: 14.3826 +/- 0.0212853
Gauss 3 Mean: 19.3707 +/- 0.0253728
Gauss 4 Mean: 24.4439 +/- 0.0236481
Delta E1: 5.00997
Delta E2: 4.98807
Delta E3: 5.0732
Delta E4: 5.25356
Sigma E1: 0.0296752
Sigma E2: 0.0331186
Sigma E3: 0.0346844
Sigma E4: 0.0351995
Weighted mean of delta E values: 5.07216
Mean of sigma E values: 0.0164719

//166-1.71-1.61
Gauss 1 Mean: 9.43535 +/- 0.0222692
Gauss 2 Mean: 14.5273 +/- 0.020911
Gauss 3 Mean: 19.4236 +/- 0.0249948
Gauss 4 Mean: 24.5917 +/- 0.023654
Delta E1: 5.09198
Delta E2: 4.89631
Delta E3: 5.16803
Delta E4: 5.12633
Sigma E1: 0.0305481
Sigma E2: 0.0325885
Sigma E3: 0.034413
Sigma E4: 0.0344182
Weighted mean of delta E values: 5.06739
Mean of sigma E values: 0.0164359

//166-1.42-1.61
Gauss 1 Mean: 9.57207 +/- 0.021617
Gauss 2 Mean: 14.7544 +/- 0.0103668
Gauss 3 Mean: 19.6488 +/- 0.037
Gauss 4 Mean: 24.8182 +/- 0.0246518
Delta E1: 5.18238
Delta E2: 4.89431
Delta E3: 5.16941
Delta E4: 5.2031
Sigma E1: 0.0239743
Sigma E2: 0.0384249
Sigma E3: 0.0444602
Sigma E4: 0.0356874
Weighted mean of delta E values: 5.13238
Mean of sigma E values: 0.0164217

//166-1.60-1.48
Gauss 1 Mean: 9.63018 +/- 0.0257068
Gauss 2 Mean: 14.5854 +/- 0.018138
Gauss 3 Mean: 19.642 +/- 0.0167065
Gauss 4 Mean: 24.9403 +/- 0.0112534
Gauss 5 Mean: 29.9037 +/- 0.0253899
Delta E1: 4.95517
Delta E2: 5.05665
Delta E3: 5.29834
Delta E4: 4.96336
Sigma E1: 0.0314615
Sigma E2: 0.0246596
Sigma E3: 0.0201431
Sigma E4: 0.027772
Weighted mean of delta E values: 5.11466
Mean of sigma E values: 0.0124845

//166-1.60-1.34
Gauss 1 Mean: 9.79878 +/- 0.0159394
Gauss 2 Mean: 14.5853 +/- 0.0157323
Gauss 3 Mean: 19.5945 +/- 0.0190724
Gauss 4 Mean: 24.7193 +/- 0.020255
Gauss 5 Mean: 29.9159 +/- 0.0248471
Delta E1: 4.78649
Delta E2: 5.00918
Delta E3: 5.12485
Delta E4: 5.19664
Sigma E1: 0.0223958
Sigma E2: 0.0247237
Sigma E3: 0.0278212
Sigma E4: 0.0320569
Weighted mean of delta E values: 4.99016
Mean of sigma E values: 0.0130247

//166-1.60-1.17
Gauss 1 Mean: 9.66541 +/- 0.0211336
Gauss 2 Mean: 14.4578 +/- 0.0225063
Gauss 3 Mean: 19.6304 +/- 0.0183684
Gauss 4 Mean: 24.7854 +/- 0.0168024
Gauss 5 Mean: 29.9645 +/- 0.017303
Delta E1: 4.79239
Delta E2: 5.17258
Delta E3: 5.15497
Delta E4: 5.17912
Sigma E1: 0.0308734
Sigma E2: 0.0290505
Sigma E3: 0.0248941
Sigma E4: 0.0241188
Weighted mean of delta E values: 5.09784
Mean of sigma E values: 0.0134029

//170-1.60-1.61
Gauss 1 Mean: 9.69643 +/- 0.0253421
Gauss 2 Mean: 14.6892 +/- 0.0221438
Gauss 3 Mean: 19.7232 +/- 0.0227953
Gauss 4 Mean: 25.0253 +/- 0.0180317
Gauss 5 Mean: 29.7319 +/- 0.0132145
Delta E1: 4.99276
Delta E2: 5.03399
Delta E3: 5.30212
Delta E4: 4.7066
Sigma E1: 0.0336537
Sigma E2: 0.0317801
Sigma E3: 0.0290648
Sigma E4: 0.0223554
Weighted mean of delta E values: 4.96002
Mean of sigma E values: 0.0140611

//181-1.60-1.61
Gauss 1 Mean: 9.76036 +/- 0.023287
Gauss 2 Mean: 14.6771 +/- 0.0246812
Gauss 3 Mean: 19.7818 +/- 0.0221939
Gauss 4 Mean: 24.9353 +/- 0.0213374
Gauss 5 Mean: 29.5923 +/- 0.0196569
Delta E1: 4.91674
Delta E2: 5.10466
Delta E3: 5.15353
Delta E4: 4.657
Sigma E1: 0.0339329
Sigma E2: 0.0331923
Sigma E3: 0.0307872
Sigma E4: 0.0290117
Weighted mean of delta E values: 4.94455
Mean of sigma E values: 0.0157735


//190-1.60-1.61
Gauss 1 Mean: 9.76965 +/- 0.024101
Gauss 2 Mean: 14.6609 +/- 0.0228826
Gauss 3 Mean: 19.7692 +/- 0.022676
Gauss 4 Mean: 24.9766 +/- 0.0197275
Gauss 5 Mean: 29.6171 +/- 0.0231422
Delta E1: 4.89121
Delta E2: 5.10837
Delta E3: 5.20735
Delta E4: 4.64051
Sigma E1: 0.0332336
Sigma E2: 0.0322151
Sigma E3: 0.0300562
Sigma E4: 0.0304094
Weighted mean of delta E values: 4.96222
Mean of sigma E values: 0.0156993

//200-1.60-1.61
Gauss 1 Mean: 9.78187 +/- 0.0297704
Gauss 2 Mean: 14.6823 +/- 0.0131432
Gauss 3 Mean: 19.6811 +/- 0.0223653
Gauss 4 Mean: 24.8373 +/- 0.0145865
Gauss 5 Mean: 30.008 +/- 0.018513
Delta E1: 4.90039
Delta E2: 4.99888
Delta E3: 5.15619
Delta E4: 5.17065
Sigma E1: 0.0325425
Sigma E2: 0.0259413
Sigma E3: 0.0267015
Sigma E4: 0.023569
Weighted mean of delta E values: 5.07643
Mean of sigma E values: 0.0133238

//HISTO
   1  Constant     4.36194e+00   1.86884e+00   6.73695e-04  -7.17491e-05
   2  Mean         4.99999e+00   1.01952e-01   2.76157e-05  -3.66808e-03
   3  Sigma        1.20120e-01   1.11257e-01   9.44010e-05  -5.99934e-04
Mean value of the Gaussian Fit: 4.99999 +/- 0.12012




