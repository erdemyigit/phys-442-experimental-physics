void closed(){

  const int len = 16;
  
  float x[len];

  for(int i=0; i<len; i++){
    x[i] = 0.5*i;
  }

  float y[len] = {26,4,26,36,10,8,32,28,4,22,40,24,2,30,36,4};

  float sx[len], sy[len];

  for(int i=0; i<len; i++){
    sx[i] = 0.1;
    sy[i] = 2;
  }

  //Canvas Creation and Modification
  TCanvas *c1 = new TCanvas();
  c1->SetTickx();
  c1->SetTicky();
  c1->SetGridx();
  c1->SetGridy();

  //Plotting the distance vs. current graph
  TGraphErrors *gr = new TGraphErrors(len,x,y,sx,sy);

  gr->SetMarkerColor(kBlue);
  gr->SetLineWidth(1);
  gr->SetTitle("Distance (cm) vs. Current (micro Ampere)");
  gr->GetXaxis()->SetTitle("Distance (cm)");
  gr->GetYaxis()->SetTitle("Current (micro Ampere)");
  gr->Draw("A*");

  //Fittin part
  TF1 *fitfunc = new TF1("fitfunc", "[0]+[1]*exp([2]*x)*sin([3]*x+[4])",0,8);

  fitfunc->SetParameter(0,25);
  fitfunc->SetParameter(1,10);
  fitfunc->SetParameter(2,0.1);
  fitfunc->SetParameter(3,2.2);
  fitfunc->SetParameter(4,0.2);
  
  fitfunc->SetLineWidth(3);
  fitfunc->SetLineColor(kRed);
  gr->Fit(fitfunc, "R");

  float max1 = fitfunc->GetMaximumX(0,2);
  float max2 = fitfunc->GetMaximumX(2,4);
  float max3 = fitfunc->GetMaximumX(4,8);

  cout << max1 << " " << max2 << " " << max3 << endl;

  float dif1 = max2 - max1;
  float dif2 = max3 - max2;

  //Calculating the wavelength

  float lambda = (dif1+dif2)/2;

  cout << "The wavelength is: " << lambda << endl;   
}
