void decaymode(){

  ifstream file;
  file.open("Data/erdem_decay.txt"); //Input file
  if(!file) { //File couldn’t be opened
    cerr << "Error: File could not be opened." << endl;
    exit(1);
  }

  int line = 3570;

  float x[line], y[line];
  float x_dat, y_dat;

  int count = 0;

  while(file.good()){
    file >> x_dat >> y_dat ;
    
    x[count]=x_dat;
    y[count]=y_dat ;
    count++;
  }

  cout << "Number of lines: " << count << endl; //In order to check if we read all of the data 

									    file.close(); //We should always close the file

  //Canvas Creation and Modification
  TCanvas *c1 = new TCanvas();
  c1->SetTickx();
  c1->SetTicky();
  c1->SetGridx();
  c1->SetGridy();

  //Plotting the Time(s) vs. Voltage(V) Graph

  TGraphErrors *gr = new TGraphErrors(line,x,y,0,0);

  gr->SetLineColor(kBlue);
  gr->SetLineWidth(2);
  gr->SetTitle("");
  gr->GetXaxis()->SetTitle("Time (s)");
  gr->GetYaxis()->SetTitle("Voltage (V)");
  gr->Draw("AC");

  //Defining the Fit function
  TF1 *fitfunc = new TF1("fitfunc","[0]+[1]*exp(-[4]*(x))*sin([2]*(x)+[3])",0,1000);
  //Setting Parameters
  fitfunc->SetParameter(0,3.21);
  fitfunc->SetParameter(1,0.09);
  fitfunc->SetParameter(2,0.05);

  fitfunc->SetLineColor(kRed);
  fitfunc->SetLineWidth(4);
  gr->Fit(fitfunc, "R");

  //Extracting the fit parameters
  float C = fitfunc->GetParameter(0);
  float D = fitfunc->GetParameter(1);
  float wd = fitfunc->GetParameter(2);
  float psi = fitfunc->GetParameter(3);
  float damp = fitfunc->GetParameter(4);
  
  //We can add a custom legend to make the data more readable
  TLegend *leg1 = new TLegend(0.7, 0.7, 0.9, 0.9);
  leg1->AddEntry(gr, "Measured Data", "l");
  leg1->AddEntry(fitfunc, "Fit Function", "l");
  leg1->Draw();

  //Printing out the extracted parameters
  cout << "C (V) is " << C << endl;
  cout << "D (V) is " << D << endl;
  cout << "Damping frequency is " << wd << endl;
  cout << "Phase constant is " << psi << endl;
  cout << "Damping constant is " << damp << endl;
}
