void _4000V_2s(){

  ifstream file;
  file.open("Data/4000V,2s.txt"); //Input file

  if(!file) { //File couldn't be opened
    cerr << "Error: File could not be opened." << endl;
    exit(1);
  }

  float data;
  int line = 24; //We have to manually enter the number of lines in the data file. In order to prevent this, we could also use a float vector but in that case, we would then need to convert the vector into an array again because TGraphError does not accept a vector type as a parameter 
  float time[line];

  int i=0;
  //Reading the data into our time array
  while(file.good()){
    file >> data;

    time[i] = data;
    i++;
  }
  file.close(); //We should always close the file

  //We can check if we read the data correctly
  cout << "Time data:" << endl;
  for(int i=0; i<line; i++){
    cout << time[i] << " ";
  }
  cout << endl;

  //Now, we shift the first element of the array to the origin (x=0)
  float shift = time[0];
  for(int i=0; i<line; i++){
    time[i] -= shift;
  }

  //We can check again if we shifted the coordinate correctly
  cout << "Time data:" << endl;
  for(int i=0; i<line; i++){
    cout << time[i] << " ";
  }
  cout << endl;

  float x[line-1]; //This will be the x axis (Time (s)) in our TGraphErrors instance
  float y[line-1]; //This will be the y axis (Hertz (1/s)) in our TGraphErrors instance

  //x axis will contain the middle points of the time intervals 
  for(int i=0; i<line-1; i++){
    x[i] = (time[i]+time[i+1])/2;
  }

  //Calculating the charging periods and forming a new array
  float chargPer[line-1];

  for(int i=0; i<line-1; i++){
    chargPer[i] = time[i+1]-time[i];
  }

  //y axis will contain 1/charging periods
  for(int i=0; i<line-1; i++){
    y[i] = 1/chargPer[i];
  }

  float unc_x[line-1]; //This array will contain the uncertainty in the x values
  float unc_y[line-1]; //This array will contain the uncertainty in the y values

  float unc_h = 0.3; //We assume the uncertainty due to human error to be 0.3 s
  float unc_inst = 1; //Uncertainty due to our instrument is given to be 1s
    
  float sx = sqrt(pow(unc_h,2)+pow(unc_inst,2));

  //Using the error propagation formula, we find that the uncertainty of all x variables are the same
  for(int i=0; i<line-1; i++){
    unc_x[i] = sx;
  }

  //Using the error propagation formula, we find that the uncertainty off y variables varies as a function of y
  for(int i=0; i<line-1; i++){
    unc_y[i] = 2*sx*pow(y[i],2);
  }

  //Creating a canvas
  TCanvas *c1 = new TCanvas();
  c1->SetTickx();
  c1->SetTicky();
  c1->SetGridx();
  c1->SetGridy();

  TGraphErrors *gr = new TGraphErrors(line-1,x,y,unc_x,unc_y);
  
  gr->SetMarkerColor(kBlue);
  gr->SetLineWidth(1);
  gr->SetTitle("1/Charging Periods vs. Average Times Graph");
  gr->GetXaxis()->SetTitle("T (s)");
  gr->GetYaxis()->SetTitle("Hertz (1/s)");
  gr->Draw("A*");

  // gStyle->SetOptFit(1111); //This will determine the type of information about our fit parameters printed in the statistics box (parameters are "pcev" for probability, chi^2/dof, errors, name/values of parameters, respectively)

  //Fitting exponential function to our graph

  TF1 *exp = new TF1("exp", "expo");
    
  gr->Fit(exp);

  //Extracting the parameters
  float slope = exp->GetParameter(1);
  float slope_err = exp->GetParError(1);

  float chi2 = exp->GetChisquare();
  float dof = exp->GetNDF();
  float chi2_dof = chi2/dof;

  cout << "Decay constant is: " << slope << " +/- " << slope_err << endl;
  cout << "Chi2/DoF is: " << chi2_dof << endl;

  TLegend *leg1 = new TLegend(0.7, 0.7, 0.9, 0.9);
  leg1->AddEntry(gr, "Measured Data", "p");
  leg1->AddEntry(exp, "Exponential Fit", "l");
  leg1->Draw();
}
