void halfLife_numsq(){

  float lambda_2[4] = {0.0122025,0.0111105,0.0107477,0.0126689};
  float lambda_3[4] = {0.0119856,0.0139962,0.0146388,0.0126464};
  float lambda_4[4] = {0.0119100,0.0095842,0.0129544,0.0104838};
  float lambda_5[4] = {0.0107271,0.0115279,0.0094027,0.0089935};

  float sigma_lambda_2[4] = {0.0003490,0.0003258,0.0002929,0.0003217};
  float sigma_lambda_3[4] = {0.0004195,0.0005499,0.0008465,0.0004568};
  float sigma_lambda_4[4] = {0.0002736,0.0005988,0.0007703,0.0005398};
  float sigma_lambda_5[4] = {0.0005010,0.0004722,0.0004004,0.0001906};

  float tot1=0,tot2=0,tot3=0,tot4=0;
  float sum1=0,sum2=0,sum3=0,sum4=0;
  float wmean1,wmean2,wmean3,wmean4;
  float sigma1,sigma2,sigma3,sigma4;

  for(int i=0; i<4; i++){
    tot1 += 1/pow(sigma_lambda_2[i],2);
    tot2 += 1/pow(sigma_lambda_3[i],2);
    tot3 += 1/pow(sigma_lambda_4[i],2);
    tot4 += 1/pow(sigma_lambda_5[i],2);
  }

  for(int i=0; i<4; i++){
    sum1 += lambda_2[i]/pow(sigma_lambda_2[i],2);
    sum2 += lambda_3[i]/pow(sigma_lambda_3[i],2);
    sum3 += lambda_4[i]/pow(sigma_lambda_4[i],2);
    sum4 += lambda_5[i]/pow(sigma_lambda_5[i],2);
  }

  wmean1 = sum1/tot1;
  sigma1 = sqrt(1/tot1);

  wmean2 = sum2/tot2;
  sigma2 = sqrt(1/tot2);

  wmean3 = sum3/tot3;
  sigma3 = sqrt(1/tot3);

  wmean4 = sum4/tot4;
  sigma4 = sqrt(1/tot4);

  //Calculating half life values for the 4 cases
  float halflife_1 = log(2)/wmean1;
  float halflife_sigma1 = log(2)*sigma1/pow(wmean1,2);

  float halflife_2 = log(2)/wmean2;
  float halflife_sigma2 = log(2)*sigma2/pow(wmean2,2);

  float halflife_3 = log(2)/wmean3;
  float halflife_sigma3 = log(2)*sigma3/pow(wmean3,2);

  float halflife_4 = log(2)/wmean4;
  float halflife_sigma4 = log(2)*sigma4/pow(wmean4,2);

  cout << "Half life for 2 squeezes is " << halflife_1 << " +/- " << halflife_sigma1 << endl;
  cout << "Half life for 3 squeezes is " << halflife_2 << " +/- " << halflife_sigma2 << endl;
  cout << "Half life for 4 squeezes is " << halflife_3 << " +/- " << halflife_sigma3 << endl;
  cout << "Half life for 5 squeezes is " << halflife_4 << " +/- " << halflife_sigma4 << endl;

  //Now, let's plot number of squeezes vs. half life graph

  float x[4] = {2,3,4,5};
  float y[4] = {halflife_1,halflife_2,halflife_3,halflife_4};

  float sy[4] = {halflife_sigma1,halflife_sigma2,halflife_sigma3, halflife_sigma4};

  //Creating a canvas
  TCanvas *c1 = new TCanvas();
  c1->SetTickx();
  c1->SetTicky();
  c1->SetGridx();
  c1->SetGridy();

  TGraphErrors *gr = new TGraphErrors(4,x,y,0,sy);

  gr->SetMarkerColor(kBlue);
  gr->SetLineWidth(1);
  gr->SetTitle("Number of squeezes vs. Half-life Graph");
  gr->GetXaxis()->SetTitle("Number of squeezes");
  gr->GetYaxis()->SetTitle("Half-life (s)");
  gr->Draw("A*");

  //Drawing the theoretical value of 55.6 as a straight line
  TLine *trueval = new TLine(1.7, 55.6, 5.3, 55.6);
  trueval->SetLineColor(kRed);
  trueval->SetLineWidth(2);
  trueval->Draw("SAME");
}
