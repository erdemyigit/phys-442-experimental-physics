# PHYS 442 - Experimental Physics

This repository contains the ROOT (C++ based data analysis framework) macros that I have written, data sets that I have used, and the reports that I have prepared for various experiments for the PHYS 442 (Experimental Physics) course at Bogazici University in Spring 2022. 

## Experiment 1 - Poisson Statistics

## Experiment 2 - Franck-Hertz 

## Experiment 3 - Radioactive Decay

## Experiment 4 - Scattering in Two Dimensions

## Experiment 5 - Cavendish Experiment

## Experiment 6 - Microwave Experiments
